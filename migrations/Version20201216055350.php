<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20201216055350 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Add roles and username index';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE user ADD roles LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\'');
        $this->addSql('CREATE INDEX idx_username ON user (username)');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('DROP INDEX idx_username ON user');
        $this->addSql('ALTER TABLE user DROP roles');
    }
}
