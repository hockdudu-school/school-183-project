<?php


namespace App\Controller\Admin;


use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UserAdminController extends AbstractController
{

    /**
     * @Route("/admin/user", name="admin_user_index")
     */
    public function index(UserRepository $userRepository): Response
    {
        $users = $userRepository->findBy([], [
            'username' => 'ASC',
        ]);

        return $this->render('admin/user.html.twig', [
            'users' => $users,
        ]);
    }
}