<?php

namespace App\EventSubscriber;

use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Http\Event\LogoutEvent;

class LogoutSubscriber implements EventSubscriberInterface
{
    private $logger;

    public function __construct(LoggerInterface $authLogger)
    {
        $this->logger = $authLogger;
    }

    public static function getSubscribedEvents()
    {
        return [
            LogoutEvent::class => [
                ['onLogout', 0],
            ],
        ];
    }

    public function onLogout(LogoutEvent $event)
    {
        $this->logger->notice('Logout', [
            'username' => $event->getToken()->getUsername(),
        ]);
    }
}