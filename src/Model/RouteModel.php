<?php

namespace App\Model;

class RouteModel
{
    private $route;
    private $name;

    public function __construct(string $route, string $name)
    {
        $this->route = $route;
        $this->name = $name;
    }

    public function getRoute(): string
    {
        return $this->route;
    }

    public function getName(): string
    {
        return $this->name;
    }
}