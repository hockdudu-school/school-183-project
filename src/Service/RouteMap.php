<?php


namespace App\Service;


use App\Model\RouteModel;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class RouteMap
{
    private $authorizationChecker;

    public function __construct(AuthorizationCheckerInterface $authorizationChecker)
    {
        $this->authorizationChecker = $authorizationChecker;
    }

    public function getNavbarRoutes(): array
    {
        $routes = [
            new RouteModel('index', 'Home'),
        ];

        if ($this->authorizationChecker->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $routes[] = new RouteModel('member', 'Member');
        }

        if ($this->authorizationChecker->isGranted('ROLE_ADMIN')) {
            $routes[] = new RouteModel('admin_index', 'Admin');
        }

        return $routes;
    }

    public function getLoginButtons(): array
    {
        if ($this->authorizationChecker->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return [
                new RouteModel('app_logout', 'Logout')
            ];
        } else {
            return [
                new RouteModel('app_login', 'Login'),
                new RouteModel('app_register', 'Register'),
            ];
        }
    }
}