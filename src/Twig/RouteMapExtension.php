<?php


namespace App\Twig;


use App\Service\RouteMap;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class RouteMapExtension extends AbstractExtension
{
    private $routeMap;

    public function __construct(RouteMap $routeMap)
    {
        $this->routeMap = $routeMap;
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('getNavbarRoutes', [$this->routeMap, 'getNavbarRoutes']),
            new TwigFunction('getLoginButtons', [$this->routeMap, 'getLoginButtons']),
        ];
    }
}